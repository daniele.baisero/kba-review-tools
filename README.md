# kba-review-tools

Minor tools to aid in the KBA review process

## Instructions to use the tools:

To use the tools, simply download the executables in one of the distribution folders (linux, osx, windows) and double-click on the file. There is no need to install anything.

## Instructions to create the executables:

If you wish to compile the exectuables yourself, you will need to install the "webbrowser" and "pyinstaller" packages.

python3 -m pip install webbrowser  
python3 -m pip install pyinstaller

-or-

python -m pip install webbrowser  
python -m pip install pyinstaller

### WARNING
Before installing, ensure that the python scripts work on your system.
Pyinstaller will not check that all necessary libraries are available.

### Linux
pyinstaller -w --clean --distpath linux -F goto_kba.py
pyinstaller -w --clean --distpath linux -F goto_species.py

### OSX
pyinstaller -w --clean --distpath osx -F goto_kba.py
pyinstaller -w --clean --distpath osx -F goto_species.py

### Windows
pyinstaller -w --clean --distpath windows -F goto_kba.py
pyinstaller -w --clean --distpath windows -F goto_species.py
