#!/usr/bin/python3

import tkinter as tk
from tkinter import messagebox
from webbrowser import open_new_tab


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        # configure the root window
        self.title('goto:species')
        self.geometry('')

        # label
        self.label = tk.Label(self, text='Species : ')
        self.label.grid(row=0, column=0, padx=(10, 0), pady=(10, 5))

        # field
        self.field = tk.Entry(self, width=30)
        self.field.focus_set()
        self.field.grid(row=0, column=1, padx=(0, 10), pady=(10, 5))

        # info
        self.aboutButton = tk.Button(self, text='About')
        self.aboutButton['command'] = self.about
        self.aboutButton.grid(row=1, column=0, padx=(10, 0), pady=(5, 10))

        # button
        self.button = tk.Button(self, text='Enter')
        self.button['command'] = self.goto
        self.button.grid(row=1, column=1, padx=(10, 10), pady=(5, 10))

        # keybindings
        self.bind('<Return>', self.goto)
        self.bind('<Escape>', lambda x: self.destroy())

    def goto(self, species = None):
        species = self.field.get()
        if species == '':
            open_new_tab('https://www.iucnredlist.org/')
        else:
            try:
                int(species)
                open_new_tab(f'https://apiv3.iucnredlist.org/api/v3/taxonredirect/{species}')
            except:
                open_new_tab(f'https://apiv3.iucnredlist.org/api/v3/website/{species}')
        self.destroy()

    def about(self):
        messageText = '''Created by:
        Daniele Baisero, 2021

This program comes with absolutely no warranty.

Enter a scientific name or taxon ID and go to the species' IUCN page.'''
        messagebox.showinfo(title='About goto:species', message=messageText)


if __name__ == "__main__":
    app = App()
    app.eval('tk::PlaceWindow . center')
    app.resizable(False, False) 
    app.mainloop()
