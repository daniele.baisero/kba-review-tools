#!/usr/bin/python3

import tkinter as tk
from tkinter import messagebox
from webbrowser import open_new_tab


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        # configure the root window
        self.title('goto:kba')

        # label
        self.label = tk.Label(self, text='KBA : ')
        self.label.grid(row=0, column=0, padx=(10, 0), pady=(10, 5))

        # field
        self.field = tk.Entry(self, width=30)
        self.field.focus_set()
        self.field.grid(row=0, column=1, padx=(0, 10), pady=(10, 5))

        # info
        self.aboutButton = tk.Button(self, text='About')
        self.aboutButton['command'] = self.about
        self.aboutButton.grid(row=1, column=0, padx=(10, 0), pady=(5, 10))

        # button
        self.button = tk.Button(self, text='Enter')
        self.button['command'] = self.goto
        self.button.grid(row=1, column=1, padx=(0, 10), pady=(5, 10))

        # keybindings
        self.bind('<Return>', self.goto)
        self.bind('<Escape>', lambda x: self.destroy())

    def goto(self, kba = None):
        kba = self.field.get()
        if kba == '':
            open_new_tab('http://www.keybiodiversityareas.org/sites/search')
        else:
            open_new_tab(f'http://www.keybiodiversityareas.org/site/factsheet/{kba}')
        self.destroy()

    def about(self):
        messageText = '''Created by:
Daniele Baisero, 2021

This program comes with absolutely no warranty.

Enter a KBA ID and go to its factsheet page.'''
        messagebox.showinfo(title='About goto:kba', message=messageText)


if __name__ == "__main__":
    app = App()
    app.eval('tk::PlaceWindow . center')
    app.resizable(False, False) 
    app.mainloop()
